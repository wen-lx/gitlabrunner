FROM node:lts-alpine3.12 as build

RUN npm config set registry https://registry.npm.taobao.org

WORKDIR /tmp/cache

ADD package.json .
ADD package-lock.json .
RUN npm install

ADD . .
RUN npm run build

FROM socialengine/nginx-spa:latest as nginx

COPY --from=build /tmp/cache/ssl.conf /etc/nginx/conf.d/default.conf
COPY --from=build /tmp/cache/dist /app
