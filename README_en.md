# seeta-enterprise (智慧园区企业版)

## Project setup
```bash
npm install
```

### Compiles and hot-reloads for development
```bash
npm run serve
```

### Compiles and minifies for production
```bash
npm run build
```

### Customize configuration
- Modify Backend Url

```
// public/config.js
window.g = {
  baseURL: 'http://192.168.0.110:9090/' // your url with backend
}
```