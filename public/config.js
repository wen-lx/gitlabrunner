// 开发环境后台接口地址
window.g = {
  i18n: true, // 是否开启多语言切换
  defaultLang: 'zh', // 目前支持 zh / jpn
  loginType: 'phone', // 登录方式 支持 email / phone
  baseURL: 'https://hzdev.seetatech.com:8787/api',
  userURL: 'https://hzdev.seetatech.com:4434/#/register/',
  wsUrl: 'wss://hzdev.seetatech.com:8787/api/ws'
}

window.i18n = {
  zh: {
    'title': '智慧园区'
  },
  jpn: {
    'title': 'スマートコミュニティ'
  }
}
