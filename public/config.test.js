// 测试环境后台接口地址
window.g = {
  i18n: false, // 是否开启多语言切换
  defaultLang: 'zh', // 目前支持 zh / jpn
  loginType: 'phone', // 登录方式 支持 email / phone
  baseURL: 'https://hztest.seetatech.com:8797/api',
  userURL: 'https://hztest.seetatech.com:4435/#/register/',
  wsUrl: 'wss://hztest.seetatech.com:8797/api/ws'
}

window.i18n = {
  zh: {
    'title': '智慧园区'
  },
  jpn: {
    'title': 'スマートコミュニティ'
  }
}
