import * as api from './client'
import axios from 'axios'
import Router from '@/router/index.js'
import { Notification } from 'element-ui'
import store from '@/store/index.js' // 引入状态管理
import i18n from '@/i18n/index' // 引入多语言
import { baseUrl } from './config/baseUrl'

// let pending = []
// let CancelToken = axios.CancelToken
// let cancelPending = (config) => {
//   pending.forEach((item, index) => {
//     if (config) {
//       if (item.u === config.url) {
//         item.f() // 取消请求
//         pending.splice(index, 1) // 移除当前请求记录
//       }
//     } else {
//       item.f() // 取消请求
//       pending.splice(index, 1) // 移除当前请求记录
//     }
//   })
// }
// 一、请求拦截器
axios.interceptors.request.use((config) => {
  store.commit('openIsSubmitting') // 更改为提交中
  let currentOrganization = sessionStorage.getItem('currentOrganization') && JSON.parse(sessionStorage.getItem('currentOrganization'))
  config.headers['Organization-Id'] = currentOrganization && currentOrganization.id
  // 下载接口设置headers
  if (store.state.blob) {
    config.headers['accept'] = 'application/octet-stream'
  }
  // GET请求去掉传空的参数
  let urlBase = config.url.split('?')[0]
  let urlQueryArr = Array.from(new URLSearchParams(config.url.split('?')[1]))
  let result = '?'
  for (const [key, value] of urlQueryArr) {
    if (value) {
      result = `${result}${result === '?' ? '' : '&'}${key}=${value}`
    }
  }
  config.url = urlQueryArr.length ? urlBase + result : config.url
  // 用户多次点击，返回最后一次请求，取消之前的请求。
  // cancelPending(config)
  // config.cancelToken = new CancelToken((c) => {
  //   pending.push({ 'u': config.url, 'f': c })
  // })
  return config
}, (error) => {
  console.log(error)
})

// 二、响应拦截器
axios.interceptors.response.use((res) => {
  store.commit('closeIsSubmitting') // 更改为提交完成
  // token过期，返回登录页
  return res
}, (error) => {
  store.commit('closeIsSubmitting') // 更改为提交完成
  // 对响应错误做点什么
  if (!error.response) {
    return false
  }
  // 人员不存在时，直接跳转登录页
  if ((error.response.status === 403 && error.response.data.message === 'user not authorized') || (error.response.status === 500 && error.response.data.message === 'user not found')) {
    sessionStorage.removeItem('username')
    if (Router.currentRoute.fullPath !== '/admin-login') {
      Router.push({ name: 'Exception', params: { type: error.response.status } })
    }
  }
  // 开关门时无需再提示信息
  const patt = /relayClose|relayOpen/
  if (!patt.test(error.response.config.url)) {
    // @ts-ignore
    notificationError(i18n.t('alert.error'), error.response.data.message)
  }
  // 登录信息过期
  if (error.response.status === 401) {
    if (Router.currentRoute.fullPath === '/admin-login' || sessionStorage.getItem('namespace') === 'admin') {
      Router.push('/admin-login')
    } else {
      Router.push('/')
    }
  }
  // return Promise.reject(error)
  // throw new Error('重置错误状态')
  // throw (error)
  // 返回错误信息
  return error
})

function notificationError (title: any, msg: any, duration: number) {
  Notification.error({
    title: title,
    message: msg,
    duration: duration !== null ? duration : 1500
  })
}
const configuration = new api.Configuration({
  // basePath: process.env.API_URL,
  basePath: baseUrl,

  apiKey () {
    // TODO: 从localStorage中获取Token,Organization-Id
    return sessionStorage.getItem('id')
  }
})

/* accessLogsGet */
export const accessLogsApi = new api.AccessLogsApi(configuration)

/* actionReourcesGet, actionReourcesIdDelete, actionReourcesIdPatch, actionReourcesPost */
export const actionResourcesApi = new api.ActionResourcesApi(configuration)

/* actionResourceGroupsGet, actionResourceGroupsIdDelete, actionResourceGroupsIdPatch, actionResourceGroupsPost */
export const actionResourceGroupsApi = new api.ActionResourceGroupsApi(configuration)

/* addressesGet, addressesIdDelete, addressesIdPatch, addressesPost */
export const addressesApi = new api.AddressesApi(configuration)

/* adminRolesGet, adminRolesIdDelete, adminRolesIdPatch, adminRolesPost */
// export const adminRolesApi = new api.AdminRolesApi(configuration)

/* adminsGet, adminsIdDelete, adminsIdPatch, adminsIdRoleGet, adminsLoginPost, adminsPost  */
export const adminsApi = new api.AdminsApi(configuration)

/* applicationsGet, applicationsIdDelete, applicationsIdPatch, applicationsPost */
export const applicationsApi = new api.ApplicationsApi(configuration)

/* businessesGet, businessesIdDelete, businessesIdPatch, businessesPost */
export const businessesApi = new api.BusinessesApi(configuration)

export const conferenceRoomsApi = new api.ConferenceRoomsApi(configuration)

export const deviceLogsApi = new api.DeviceLogsApi(configuration)

/* deviceTypesGet, deviceTypesIdDelete, deviceTypesIdPatch, deviceTypesPost */
export const deviceTypesApi = new api.DeviceTypesApi(configuration)

/* devicesCodeDelete, devicesCodeLogsGet, devicesCodePatch, devicesCodeTestPost, devicesGet, devicesPost, devicesCodeCamerasPost, devicesCodeCamerasCameraIdPatch, devicesCodeCamerasCameraIdDelete, devicesCodeApkPost */
export const devicesApi = new api.DevicesApi(configuration)

export const invitationsApi = new api.InvitationsApi(configuration)

/* leavesGet, leavesIdDelete, leavesIdPatch, leavesPost */
export const leavesApi = new api.LeavesApi(configuration)

/* membersGet */
export const membersApi = new api.MembersApi(configuration)

/* organizationRolesGet, organizationRolesIdDelete, organizationRolesIdPatch, organizationRolesPost */
export const organizationRolesApi = new api.OrganizationRolesApi(configuration)

/* organizationsGet, organizationsIdAttendanceConfigGet, organizationsIdAttendanceConfigPatch, organizationsIdDelete, organizationsIdPatch, organizationsPost, organizationsIdSyncUsersPost */
export const organizationsApi = new api.OrganizationsApi(configuration)

/* reservationsGet, reservationsIdDelete, reservationsIdPatch, reservationsPost */
export const reservationsApi = new api.ReservationsApi(configuration)

/* stylesGet, stylesIdDelete, stylesIdPatch, stylesPost */
export const stylesApi = new api.StylesApi(configuration)

/* supplementSignsGet, supplementSignsIdDelete, supplementSignsIdPatch, supplementSignsPost */
export const supplementSignsApi = new api.SupplementSignsApi(configuration)

/* timeTemplatesGet, timeTemplatesIdDelete, timeTemplatesIdPatch, timeTemplatesPost */
export const timeTemplatesApi = new api.TimeTemplatesApi(configuration)

/* usersGet, usersIdDelete, usersIdPatch, usersIdRoleGet, usersLoginPost, usersPost, usersIdPasswordPut */
export const usersApi = new api.UsersApi(configuration)

/* visitorAppliesGet, visitorAppliesIdDelete, visitorAppliesIdPatch, visitorAppliesPost */
export const visitorAppliesApi = new api.VisitorAppliesApi(configuration)

/* uploadFile */
export const fileApi = new api.UploadsApi(configuration)

/* attendancesGet */
export const attendancesApi = new api.AttendancesApi(configuration)

/* systemConfigGet, systemConfigPatch */
export const systemConfigsApi = new api.SystemConfigsApi(configuration)

/* passRecordsGet */
export const passRecordsApi = new api.PassRecordsApi(configuration)

/* 展示字段 */
export const settingsApi = new api.SettingsApi(configuration)

/* tagsApi */
export const tagsApi = new api.TagsApi(configuration)

/* levelsApi */
export const levelsApi = new api.LevelsApi(configuration)

/* areaGroupsApi */
export const areaGroupsApi = new api.AreaGroupsApi(configuration)

/* areasApi */
export const areasApi = new api.AreasApi(configuration)

/* positionApi */
export const positionApi = new api.PositionsApi(configuration)
