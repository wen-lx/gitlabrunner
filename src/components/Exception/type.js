import i18n from '@/i18n/index'
const types = {
  403: {
    img: require('./403.svg'),
    title: '403',
    desc: i18n.t('app.403')
  },
  404: {
    img: require('./404.svg'),
    title: '404',
    desc: i18n.t('app.404')
  },
  500: {
    img: require('./500.svg'),
    title: '500',
    desc: i18n.t('app.500')
  }
}

export default types
