import Vue from 'vue'
import VueI18n from 'vue-i18n'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import jaLocale from 'element-ui/lib/locale/lang/ja'
import zh from './lang/zh.js'
import jpn from './lang/jpn.js'
Vue.use(VueI18n)

localStorage.locale = localStorage.locale ? localStorage.locale : (window.g.defaultLang || 'zh')
let language = localStorage.locale || 'zh'
const i18n = new VueI18n({
  locale: language || 'zh',
  messages: {
    zh: {
      ...zh,
      ...zhLocale,
      ...window.i18n.zh
    },
    jpn: {
      ...jpn,
      ...jaLocale,
      ...window.i18n.jpn
    }
  }
})

export default i18n
