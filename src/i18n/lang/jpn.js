const jpn = {
  // app
  'app.data-count': '合計 {total} 件',
  'app.中科视拓智慧园区': 'スマートコミュニティ',
  'app.中科院计算所智慧园区': '中科院计算所智慧园区', // TODO 02-02
  'app.智慧园区企业版 v2.1.1': '智慧园区企业版 v2.1.1', // TODO 02-02
  'app.welcome-page': 'ようこそ',
  'app.智慧园区': 'スマートコミュニティ',
  'app.welcome-use': '欢迎使用智慧园区', // TODO 02-02
  'app.back-home': '返回首页', // TODO 02-02
  'app.403': '抱歉，你无权访问该页面', // TODO 02-02
  'app.404': '抱歉，你访问的页面不存在或仍在开发中', // TODO 02-02
  'app.500': '抱歉，服务器出错了', // TODO 02-02
  // 系统信息
  'app.system-info': 'システム',
  'app.system-config': 'システム管理',
  'app.device-type': 'デバイス種類',
  'app.member-attribute': 'ユーザー属性',
  // 权限配置
  'app.permission-config': '権限設定',
  'app.permission-manage': '権限管理',
  'app.identity-manage': '身份管理', // TODO 02-02
  'app.role-manage none': '角色管理', // TODO 02-02
  // 机构配置
  'app.organization-config': '拠点設定',
  'app.organization-manage': '拠点管理',
  'app.level-manage': '拠点管理者',
  // 人员信息
  'app.member-info': 'ユーザー',
  'app.member-manage': '人员管理', // TODO 02-02
  'app.visitor-apply': '来客予約',
  // 设备信息
  'app.device-info': 'デバイス',
  'app.device-manage': 'デバイス管理',
  'app.device-style-manage': 'スタイル管理',
  'app.time-template': '時間テンプレート',
  // 考勤信息
  'app.attendance-info': '勤怠設定',
  'app.attendance-manage': '勤怠管理',
  // 按钮组
  'button.add': '新規',
  'button.confirm': '確定',
  'button.cancel': 'キャンセル',
  'button.revoke': '撤销', // TODO 02-02
  'button.reset': 'リセット',
  'button.save': '確定',
  'button.query': '検索',
  'button.search': '搜索', // TODO 02-02
  'button.action': '操作',
  'button.edit': '編集',
  'button.edit-end': '结束编辑', // TODO 02-02
  'button.detail': '详情', // TODO 02-02
  'button.delete': '削除',
  'button.tip': '確認',
  'button.export': 'エクスポート',
  'button.batch-upgrade': '一括アップデート',
  'button.export-attendance': '勤怠エクスポート',
  'button.export-visitorlog': '导出访客记录', // TODO 02-02
  'button.attendance-setting': '勤怠設定',
  'button.allow': '許可',
  'button.reject': '拒否',
  'button.open-door': '開錠',
  'button.close-door': '施錠',
  'button.open-all-door': '一括開錠',
  'button.close-all-door': '一括施錠',
  'button.reply': '応答',
  'button.style': 'テーマ',
  'button.add-stream': 'RTSP',
  'button.copy': '复制', // TODO 02-02
  // 登录
  'login': 'ログイン',
  'register': '注册', // TODO 02-02
  'login.username': 'アカウント',
  'login.password': 'パスワード',
  'login.username-required': 'ユーザー名を入力してください',
  'login.password-required': 'パスワードを入力してください',
  'login.goto-root': '去超管登录', // TODO 02-02
  'login.goto-admin': '去管理员登录', // TODO 02-02
  'login.no-account': '没有账号？', // TODO 02-02
  'login.success': 'ログイン成功',
  // 系统
  'system.title': 'システム管理',
  'system.edit-config': 'システム設定',
  'system.open-cloud-address': 'サーバーURL',
  'system.open-cloud-setting': '开放云设置', // TODO 02-02
  'system.min-face': '最小顔サイズ',
  'system.definition': '鮮明度',
  'system.mobile-editable-photo': '移动端编辑底库照片', // TODO 02-02
  'system.placeholder-tag': '请输入标签名', // TODO 02-02
  'system.max-face-angle': '最大顔角度',
  'system.device-show-field': '设备展示字段', // TODO 02-02
  'system.set-passlog-storage': '通行记录存储设置', // TODO 02-02
  'system.passlog-max-storage-time': '通行记录最大存储时间', // TODO 02-02
  'system.day': '天', // TODO 02-02
  'system.min-day': '最少3天', // TODO 02-02
  'system.passlog-max-storage-time-tip': '说明：当通行记录时间大于设置值时，自动删除', // TODO 02-02
  'system.not-set-auto-delete': '不设置自动删除策略', // TODO 02-02
  'system.not-set-auto-delete-tip': '说明：管理人员需注意服务器存储使用情况，存储容量满会导致服务不可用', // TODO 02-02

  // 设备类型
  'device-type.title': 'デバイス種類',
  'device-type.name': 'デバイス種類名',
  'device-type.add': '新增设备类型', // TODO 02-02
  'device-type.edit': 'デバイス種類編集',
  'device-type.delete-tip': 'このデバイス種類を削除しますか？',
  'device-type.name-required': 'デバイス種類名を入力してください',
  // 设备
  'device.title': 'デバイス管理',
  'device.add': '新規デバイス',
  'device.edit': 'デバイス編集',
  'device.delete-tip': 'このデバイスを削除しますか？',
  'device.name': 'デバイス名',
  'device.name-validate': '设备名称必须小于30位', // TODO 02-02
  'device.code': 'デバイスID',
  'device.type': 'デバイス種類',
  'device.status': 'デバイス状態',
  'device.version': '端末型番',
  'device.soft-version': 'APKバージョン',
  'device.address-affiliation': '所属場所',
  'device.param': 'パラメータ設定',
  'device.log-level': 'アップロード用ログレベル',
  'device.继电器对调': 'リレー逆設定',
  'device.screensavers': '受付画面',
  'device.voice': '音声',
  'device.flashing-light': 'フラッシュ',
  'device.volume': '音量',
  'device.min-face-width': '最小顔サイズ',
  'device.max-face-angle': '最大顔向き',
  'device.max-face': '最大顔',
  'device.definition': '鮮明度',
  'device.min-definition': '最小鮮明度',
  'device.resolution': '解像度',
  'device.living-detect': 'なりすまし検出',
  'device.filter-type': 'フィルタ種類',
  'device.filter-rate': 'フィルタ頻度(ms)',
  'device.画面边缘像素': '画面枠画像設定',
  'device.活体检测过滤程度': 'なりすまし認証設定',
  'device.1:n验证阈值': '1:n認証閾値',
  'device.1:n不可信阈值': '1:N信頼しない閾値',
  'device.1:1验证阈值': '1:1認証閾値',
  'device.relay-address': 'リレーアドレス',
  'device.继电器保持时长': 'リレー有効時間(s)',
  'device.sync-WeChat': 'WeChat企業版',
  'device.WeChat-SecretNo': 'WeChat SecretNo',
  'device.recognition-type': '認証種類',
  'device.alter-recognition-type': '候補認証種類',
  'device.底库比对': 'データベース照会',
  'device.存在性验证': '存在認証',
  'device.is-open': '開錠',
  'device.about-door': 'にリンクする扉？',
  'device.camera-type': 'カメラ種類',
  'device.time-template': '日時テンプレート',
  'device.show-unknow-member': '認証失敗顔アップロード',
  'device.最大尝试人脸抓拍时长': '最大顔検出時間(s)',
  'device.recognition-mode': '認識モード',
  'device.截取比例': '切取比率',
  'device.switch': '認証設定',
  'device.模型活体阈值': 'なりすまし検出閾値',
  'device.人脸检测框': '顔検出枠',
  'device.抽帧频率': 'フレーム抽出頻度(ms)',
  'device.识别时限': '認証時間制限(s)',
  'device.屏保间隔': '受付画面切替時間(フレーム)',
  'device.重复识别间隔': '顔再認証時間(回)',
  'device.屏幕唤醒灵敏度': '受付画面切替(ms)',
  'device.视拓算法人脸重检': '自社アルゴリズムで顔再検出',
  'device.relay-channel': 'リレーチャンネル',
  'device.external-device': '外接デバイス',
  'device.apk-update-tip': 'APKファイルのみ対応しますので、ご注意ください',
  'device.upload-apk': 'APKファイル',
  'device.batch-upload-apk': 'APK一括アップデート',
  'device.select-device-first': 'アップデート対象デバイスを選択してください',
  'device.upload-apk-first': '请先上传apk文件', // TODO 02-02
  'device.reset-style-tip': '是否重置该设备样式？', // TODO 02-02
  'device.selected-device': '選択済デバイス',
  'device.name-required': 'デバイス名を入力してください',
  'device.code-required': 'デバイスIDを選択してください',
  'device.type-required': '请选择设备类型', // TODO 02-02
  'device.address-affiliation-required': '请选择所属地点', // TODO 02-02
  'device.SecretNo-required': 'WeChat SecretNoを入力してください',
  'device.external-device-required': '外接デバイスを選択してください',
  'device.stream.name': '名前',
  'device.stream.open-door': 'ストリーム開錠',
  'device.stream.address': 'ストリームURL',
  'device.stream.add': '新規ストリーム',
  'device.stream.edit': 'ストリーム編集',
  'device.stream.delete-tip': '是否删除该流媒体？', // TODO 02-02
  'device.stream.name-required': 'ストリーム名を入力してください',
  'device.stream.address-required': '请输入流媒体地址', // TODO 02-02
  // 样式
  'device-style.title': 'スタイル管理',
  'device-style.type': '種類',
  'device-style.name': 'スタイル名',
  'device-style.content': '内容',
  'device-style.photo': '画像',
  'device-style.add': '新規スタイル',
  'device-style.edit': 'スタイル編集',
  'device-style.delete-tip': 'このスタイルを削除しますか？',
  'device-style.name-required': 'スタイル名を入力してください',
  'device-style.type-required': '種類を選択してください',
  'device-style.photo-required': 'テーマ画像を追加してください',
  // 时间模板
  'time-template.title': '時間テンプレート',
  'time-template.name': '時間テンプレート名',
  'time-template.describe': '詳細',
  'time-template.add': '新規時間テンプレート',
  'time-template.edit': '時間テンプレート編集',
  'time-template.pass-date-period': '通行許可期間',
  'time-template.pass-time-period': '通行許可時間帯',
  'time-template.appointed-pass-date': '通行許可期間(特例)',
  'time-template.non-use-date-period': '通行禁止期間',
  'time-template.appointed-non-use-date': '通行禁止期間(特例)',
  'time-template.remove-weekend': '週末除く',
  'time-template.delete-tip': 'この時間テンプレートを削除しますか？',
  'time-template.已设置为特定通行日期': '通行許可期間(特例)に設定しました',
  'time-template.已设置为特定停用日期': '通行禁止期間（特例）に設定しました',
  'time-template.empty-tip': '利用可能時間帯がないので、日時テンプレートを確認してください！',
  'time-template.tip': 'Tip：年始に日時テンプレートを更新してください。閏年2月29号に要注意！',
  'time-template.name-required': '時間テンプレート名を入力してください',
  'time-template.pass-date-period-required': '通行許可期間を選択してください',
  'time-template.append': '新規',
  // 权限
  'privilege.title': '権限管理',
  'privilege.privilege-name': '権限名',
  'privilege.privilege-affiliation': '所属权限', // TODO 02-02
  'privilege.group': '権限グループ',
  'privilege.group-name': '権限グループ名',
  'privilege.operate-privilege': '操作权限', // TODO 02-02
  'privilege.name': '名前',
  'privilege.connector': '接口', // TODO 02-02
  'privilege.operate': '操作', // TODO 02-02
  'privilege.tag': '标识', // TODO 02-02
  'privilege.only-superadmin': '仅超管可见', // TODO 02-02
  'privilege.add': '新增操作权限', // TODO 02-02
  'privilege.edit': '编辑操作权限', // TODO 02-02
  'privilege.group-add': '新規権限グループ',
  'privilege.group-edit': '権限グループ編集',
  'privilege.delete-tip': 'この権限を削除しますか？',
  'privilege.group-delete-tip': 'この権限グループを削除しますか？',
  'privilege.isonly-superadmin-requried': '请选择仅超管可见', // TODO 02-02
  'privilege.privilege-name-required': '権限名を入力してください',
  'privilege.name-required': '请输入名称', // TODO 02-02
  'privilege.connector-required': '请输入接口', // TODO 02-02
  'privilege.operate-required': '请输入操作', // TODO 02-02
  'privilege.tag-required': '请输入标识', // TODO 02-02
  // 身份
  'identity': '身份', // TODO 02-02
  'identity.title': '身份管理', // TODO 02-02
  'identity.name': 'ロール名',
  'identity.add': '新規ロール',
  'identity.edit': 'ロール編集',
  'identity.look': '查看人员身份', // TODO 02-02
  'identity.name-required': 'ロール名を入力してください',
  'identity.delete-tip': 'このロールを削除しますか？',
  // 机构
  'organization': '拠点',
  'organization.id': 'organization_id',
  'organization.title': '拠点管理',
  'organization.name': '拠点名',
  'organization.tags': '机构标签', // TODO 02-02
  'organization.no-tag': '暂无标签', // TODO 02-02
  'organization.all': '全部机构', // TODO 02-02
  'organization.add': '新規拠点',
  'organization.edit': '拠点編集',
  'organization.describe': '詳細',
  'organization.level-tree': '部门结构', // TODO 02-02
  'organization.sync-data-tip': '企業WeChatと同期を設定しますか？',
  'organization.delete-tip': '机构删除后无法恢复，删除前请手动清空机构所有设置，包括：机构信息、角色、人员、地点、设备、考勤、访客等。', // TODO 02-02
  'organization.sync-callback': 'コールバック',
  'organization.sync-WeChat': '企業WeChat情報同期',
  'organization.register-count': 'ユーザー数',
  'organization.WeChat': '企業WeChat',
  'organization.WeChatID': '企業WeChat ID',
  'organization.SecretNo': 'SecretNo',
  'organization.token': 'token',
  'organization.aesKey': 'aesKey',
  'organization.affiliation': '所属拠点',
  'organization.SecretNo-required': 'secretNoを入力してください',
  'organization.token-required': 'tokenを入力してください',
  'organization.aesKey-required': 'aesKeyを入力してください',
  'organization.name-required': '拠点名を入力してください',
  'organization.WeChat-required': '企業WeChatを選択してください',
  'organization.WeChatID-required': '企業WeChat IDを入力してください',
  'organization.sync-callback-required': 'コールバックを設定してください',
  'organization.affiliation-required': '所属拠点を選択してください',
  // 标签
  'tags.non-use': '停用', // TODO 02-02
  'tags.create-path': '定義方式',
  'tags.default': 'デフォルト',
  'tags.self-defined': 'カスタマイズ',
  'tags.status': 'ステータス',
  'tags.normal': '正常',
  'tags.disabled': '無効',
  'tags.pass': '承認',
  'tags.not-pass': '拒否',
  'tags.allow': '許可',
  'tags.refuse': '拒否',
  'tags.check-pending': '承認待ち',
  'tags.pending': '待处理', // TODO 02-02
  'tags.deleted': '削除',
  'tags.invalid': '無効',
  'tags.visited': '已到访', // TODO 02-02
  'tags.not-visited': '未到访', // TODO 02-02
  'tags.open': '有効',
  'tags.close': '無効',
  'tags.member': '社員',
  'tags.visitor': '来客',
  'tags.stranger': '陌生人',
  'tags.empty': 'なし',
  'tags.before-start': '未開始',
  'tags.ongoing': '進行中',
  'tags.after-end': '終了',
  'tags.screensaver': '受付画像',
  'tags.background': '背景画像',
  'tags.logo': 'logo',
  'tags.information': 'コンテンツ',
  'tags.avatar': 'プロフィル画像',
  'tags.online': 'オンライン',
  'tags.offline': 'オフライン',
  'tags.qrCode': 'QRコード',
  'tags.idCard': '身分証明書',
  'tags.icCard': 'ICカード',
  'tags.max-face-detect': '大きい顔認証',
  'tags.multi-face-detect': '多数顔認証',
  'tags.mode-living': 'なりすましモデル',
  'tags.easy-living': 'なりすまし簡単チェック',
  'tags.net-camera': 'IPカエラ',
  'tags.zkst-camera-hz': '中科視拓カメラ-hz',
  'tags.zkst-camera-hi': '中科視拓カメラ-hi',
  'tags.strict': '厳しい',
  'tags.loosen': '緩い',
  'tags.fixed-rate': '頻度固定',
  'tags.pose-assess': '骨格認識',
  'tags.definition-assess': '鮮明度測定',
  'tags.edge-face-filter': '不完全顔検出無効',
  'tags.max-face-filter': '最大顔有効',
  'tags.living-detect-filter': 'なりすまし有効',
  'tags.privilege-only-root': '仅超管可见权限', // TODO 02-02
  'tags.privilege-admin-root': '超管和机构均可见权限', // TODO 02-02
  'tags.data-count': '共 {total} 个', // TODO 02-02
  'tags.no-picture': '暂无图片', // TODO 02-02
  // 提示
  'tips.select-pleaseholder': '请选择', // TODO 02-02
  'tips.start-time': '開始時刻',
  'tips.end-time': '終了時刻',
  'tips.return-normal': '正常に戻りました',
  'tips.exception-occurred': '異常が発生しました',
  'tips.back-online': 'オンラインに戻りました',
  'tips.off-line': 'オフラインになりました',
  'tips.device-update-status': 'デバイス接続状況',
  'tips.drag-file': '将文件拖到此处，或', // TODO 02-02
  'tips.click-upload': 'ファイルを選択',
  'tips.fastest-2s-response': '2秒毎にレスポンスします',
  'tips.websocket': '当前浏览器不支持websocket！', // TODO 02-02
  'websocket.device-status': '{deviceName} デバイスは {time} {alive}',
  // 部门
  'level.management': '部门管理', // TODO 02-02
  'level.delete-reminder': '部门删除后无法恢复，删除前请手动清空部门下所有设置，包括：人员、子部门等。', // TODO 02-02
  'level.add': '添加部门', // TODO 02-02
  'level.edit': '编辑部门', // TODO 02-02
  'level.name-required': '部门名称不能为空', // TODO 02-02
  'level.name': '部门名称', // TODO 02-02
  // 人员
  'member.invite-list': '邀请列表',
  'member.identity': 'ロール',
  'member.permission': '所属权限',
  'member.permission-group': '権限グループ',
  'member.edit': 'ロール編集',
  'member.see': '查看人员身份',
  'member.select-all': '全选',
  'member.action.permission': '操作权限',
  'member.info': 'ユーザー',
  'member.management': '人员管理',
  'member.visitor.appointment': '来客予約',
  'member.staff.management': '社員管理',
  'member.staff.username': 'アカウント',
  'member.staff.username-mail': '用户名或邮箱',
  'member.staff.search-result': '搜索结果',
  'member.staff.name': '名前',
  'member.staff.mail': 'メール',
  'member.staff.password': 'パスワード',
  'member.staff.reset-password': '重置密码',
  'member.staff.confirm-password': '新パスワード（確認）',
  'member.staff.phone': '携帯番号',
  'member.staff.IC': 'ICカード',
  'member.staff.identity': '身分証明書',
  'member.staff.brief-introduction': '简介',
  'member.staff.enterprise-wechat-ID': '企業WeChat ID',
  'member.staff.cover-photo': 'カバー写真',
  'member.staff.bottom-photo': '登録写真',
  'member.staff.access-rights': '許可場所',
  'member.staff.organization': '拠点',
  'member.staff.place': '場所',
  'member.staff.attendance': '考勤',
  'member.staff.congratulation': '挨拶',
  'member.staff.pass-time-limit': '通行時間',
  'member.staff.photo': '社員写真',
  'member.staff.add': '新規社員',
  'member.staff.edit': '社員編集',
  'member.staff.see-detail': '查看详情',
  'member.staff.add-user': '新建用户',
  'member.staff.add-registered-users': '注册申请审核',
  'member.staff.invite-user': '邀请用户',
  'member.staff.photo.required': '请添加员工照片',
  'member.staff.if-delete': '是否删除该员工信息？',
  'member.staff.username-limit': '用户名必须大于等于6位且小于20位',
  'member.staff.name-limit': '姓名必须大于等于2位且小于20位',
  'member.staff.name-required': '请输入姓名',
  'member.staff.mail-required': '请输入邮箱',
  'member.staff.vaild-mail-required': '邮箱格式不正确', // TODO
  'member.staff.place-required': '请选择地点',
  'member.staff.photo-required': '请输入手机号',
  'member.staff.level-required': '请选择部门',
  'member.staff.status-required': '请选择状态',
  'member.staff.attendance-required': '请选择考勤',
  'member.staff.identity-required': '请选择身份',
  'member.staff.username-mail-required': '请输入用户名或邮箱',
  'member.staff.add-organization': '新增绑定机构',
  'member.staff.edit-organization': '编辑绑定机构',
  'member.staff.admin-can-not-delete-self': '机构管理员不能删除本人',
  'member.staff.image-detection': '图片检测中',
  'member.staff.add-request-sent': '提示：您的添加请求已发送给用户，用户同意后即可获取用户信息',
  'member.staff.copy-link-invite-user': '复制下方链接邀请用户自行注册。用户注册后您可以通过【注册申请审核】或【查看未绑定人员】进行添加。', // TODO
  'member.staff.select-level': '选择部门', // TODO
  'member.staff.level-name': '部门名称', // TODO
  'member.staff.under-level': '所在部门', // TODO
  'member.staff.pass-time-tip': '不填写则默认永久拥有通行权限', // TODO
  'member.staff.see-unbound': '查看未绑定人员',
  'member.staff.see-uncheck': '查看未审核人员', // TODO
  'member.staff.see-allmember': '查看所有人员', // TODO
  'member.staff.can-not-query': '未查询到相关信息',
  'member.staff.understand': '知道了',
  'member.department': '用户提交部门', // TODO
  'member.workSpace': '用户提交地点', // TODO
  // 访客
  'visitor.interviewee': '被访人',
  'visitor.interviewee.access': '被访人必须拥有地点通行权限',
  'visitor.phone': '携帯番号',
  'visitor.mail': 'メール',
  'visitor.identity': '身分証明書',
  'visitor.cover-photo': 'カバー写真',
  'visitor.bottom-photo': '登録写真',
  'visitor.place': '通行地点',
  'visitor.colleagues': '同行人',
  'visitor.reason': '理由',
  'visitor.appointment': '来客予約',
  'visitor.query-content': '検索内容',
  'visitor.content': '内容',
  'visitor.visitor': '来客',
  'visitor.interviewees.photo': '被访人照片',
  'visitor.visitor.photo': '来客写真',
  'visitor.start-time': '開始時刻',
  'visitor.end-time': '終了時刻',
  'visitor.pass-time-limit': '通行時間',
  'visitor.appointment.add': '新規来客予約',
  'visitor.appointment.edit': '来客予約編集',
  'visitor.expired-delete-reminder': '记录删除后无法恢复，请确认是否删除？',
  'visitor.not-expired-delete-reminder': '删除记录会导致访客无法正常来访，请确认是否删除？',
  'visitor.reminder.agree': '是否同意该访客预约信息？',
  'visitor.reminder.refuse': '是否拒绝该访客预约信息？',
  'visitor.interviewee.required': '请选择被访人',
  'visitor.visitor.name.required': '请输入访客姓名',
  'visitor.peer.name.required': '请输入同行人姓名', // TODO
  'visitor.mail.required': '请输入邮箱',
  'visitor.bottom-photo.required': '请选择底库照片',
  'visitor.peers-photo.required': '请选择同行人底库照片', // TODO
  'visitor.pass-time-limit.required': '请选择通行时限',
  'visitor.place.required': '请选择通行地点',
  'visitor.reason.required': '请输入事由',
  'visitor.status.required': '请选择状态',
  'visitor.visitor.name': '访客姓名',
  'visitor.interviewees.name': '被访人姓名',
  'visitor.peers.name': '名前', // TODO
  'visitor.peers.add': '添加同行人', // TODO
  'visitor.peers.delete': '删除此同行人', // TODO
  'visitor.image-detection': '图片检测中',
  'visitor.appointment.all': '全部人员访客预约',
  'visitor.delete': '削除',
  'visitor.maskImage.tip': '可以自由上传照片，将会在人脸识别的门禁机上显示，不上传则显示系统默认图', // TODO
  'visitor.baseImage.tip': '近期肩部以上清晰、平视、正脸、无妆容、无遮挡、无美颜、表情平静的头像照片。建议上传2张及以上照片', // TODO
  // 考勤
  'attendance.normal': '正常',
  'attendance.late': '遅刻',
  'attendance.leave-early': '早退',
  'attendance.absenteeism': '未打卡',
  'attendance.abnormal': '异常',
  'attendance.clock-in': '上班打卡',
  'attendance.clock-out': '下班打卡',
  'attendance.not-yet': '暂无',
  'attendance.working-hours': '工作时长',
  'attendance.overtime-hours': '加班时长',
  'attendance.attendance-summary': '考勤汇总',
  'attendance.tips.staff-required': '请选择员工查询考勤记录',
  'attendance.hour': '時間',
  'attendance.status': 'ステータス',
  'attendance.day': '天',
  'attendance.not-start': '未開始',
  'attendance.finished': '終了',
  'attendance.ongoing': '進行中',
  'attendance.today': '今日', // TODO 12.31
  'attendance.tomorrow': '明日', // TODO 12.31
  'attendance.info': '考勤信息',
  'attendance.management': '勤怠管理',
  'attendance.config': '考勤配置',
  'attendance.staff.name': '社員名',
  'attendance.date': '日時',
  'attendance.work-time': '出社時刻',
  'attendance.go-off-work-time': '退社時刻',
  'attendance.length-of-service': '勤務時間',
  'attendance.flexible-work': 'フレックスタイム',
  'attendance.be-late-time': '遅刻バッファ設定(min)',
  'attendance.export-attendance': '勤怠エクスポート',
  'attendance.person-select': '社員選択',
  'attendance.date-range': '時間範囲選択',
  'attendance.flexible-work.required': '请选择弹性上班',
  'attendance.be-late-time.required': '请选择迟到缓冲时间',
  'attendance.work-time.required': '请选择上班时间',
  'attendance.go-off-work-time.required': '请选择下班时间',
  'attendance.date-range.required': '请选择日期区间',
  'attendance.attendance-tips': '提示：考勤配置对已生效考勤记录不会产生影响',
  'attendance.time-limit': '迟到缓冲时间必须大于0分钟',
  'attendance.attendance': '考勤记录',
  'attendance.name': '名前',
  // 通行记录
  'traffic-record.traffic-record': '通行履歴',
  'traffic-record.query.field': '検索条件',
  'traffic-record.query.content': '検索内容',
  'traffic-record.if-pass': '通過可否',
  'traffic-record.date-range': '時間範囲',
  'traffic-record.equipment.name': 'デバイス名',
  'traffic-record.equipment.code': 'デバイスID',
  'traffic-record.username': 'アカウント',
  'traffic-record.mail': 'メール',
  'traffic-record.phone': '携帯番号',
  'traffic-record.IC': 'ICカード',
  'traffic-record.reason': '拜访原因',
  'traffic-record.colleagues': '同行人',
  'traffic-record.pass-place': '通行地点',
  'traffic-record.identity': '身分証明書',
  'traffic-record.temperature': '温度',
  'traffic-record.personn.identity': 'ユーザー種類',
  'traffic-record.bottom-photo': '登録写真',
  'traffic-record.scene-photo': '実際写真',
  'traffic-record.similarity': '類似度',
  'traffic-record.identification-type': '識別種類',
  'traffic-record.record-time': '通行時刻',
  'traffic-record.export-record': '通行履歴エクスポート',
  'traffic-record.select-device': 'デバイス選択',
  'traffic-record.select-personn-attributes': 'ユーザー属性選択',
  'traffic-record.select-if-pass': '通過可否を選ぶ',
  'traffic-record.select-date-range': '時間範囲選択',
  'traffic-record.person.name': '人员姓名',
  'traffic-record.name': '名前',
  'traffic-record.record-all': '全部人员通行记录',
  'traffic-record.pass-by-card': '刷卡',
  'traffic-record.pass-by-face': '刷脸',
  // 设备日志
  'device-log.device.log': 'デバイスログ',
  'device-log.device.name': 'デバイス名',
  'device-log.log-level': 'ログレベル',
  'device-log.date-range': '時間範囲',
  'device-log.device-code': 'デバイスID',
  'device-log.content': '内容',
  'device-log.action-time': '操作時刻',
  // 操作日志
  'operation-log.operation-log': '操作ログ',
  'operation-log.date-range': '時間範囲',
  'operation-log.username': 'アカウント',
  'operation-log.content': '内容',
  'operation-log.request-methods': '请求方法',
  'operation-log.url': 'uri',
  'operation-log.user.type': '用户类型',
  'operation-log.admin': 'システム管理者',
  'operation-log.user': '用户',
  'operation-log.action-time': '操作時刻',
  // 会议室
  'meeting-room.management': '会議室管理',
  'meeting-room.name': '会議室名',
  'meeting-room.place': '場所',
  'meeting-room.add': '新規会議室',
  'meeting-room.edit': '会議室編集',
  'meeting-room.if-delete': '是否删除该会议室？',
  'meeting-room.status-required': '请选择状态',
  // 会议
  'meeting.info': '会議予約',
  'meeting.room.management': '会議室管理',
  'meeting.management': '会議管理',
  'meeting.name': '会議名',
  'meeting.room': '会議室',
  'meeting.sponsor': '主催者',
  'meeting.participants': '参会者',
  'meeting.flex-time': 'フラックス時間(min)',
  'meeting.actual-person': '出席者',
  'meeting.time': '会議時間範囲',
  'meeting.time.start': '開始時刻',
  'meeting.time.end': '終了時刻',
  'meeting.add': '新規会議',
  'meeting.edit': '会議編集',
  'meeting.if-delete': '是否删除该会议？',
  // 请求提示
  'alert.error': 'エラー',
  'alert.admin-no-permission-run': '編集権限がありません',
  // 路由
  'app.router-error': '请检查全局路由信息!', // TODO 02-02
  'app.menu-error': '请检查菜单选项！', // TODO 02-02
  // 记录日志
  'app.record-log': 'ログ管理',
  'app.traffic-record': '通行履歴',
  'app.device-log': 'デバイスログ',
  'app.operation-log': '操作ログ',
  // 地点开门
  'app.address-open': '場所設定',
  'app.address-manage': '場所管理',
  // 登录登出
  'app.logout-tip': 'ログアウトしますか？',
  'app.logout-success': 'ログアウト成功',
  'logout': 'ログアウト',
  // 密码
  'password.change-password': 'パスワード変更',
  'password.change-password-success': 'パスワード変更成功',
  'password.old-password': 'パスワード',
  'password.new-password': '新パスワード',
  'password.confirm-password': '新パスワード（確認）',
  'password.old-password-required': 'パスワードを入力してください',
  'password.new-password-required': '新パスワードを入力してください',
  'password.confirm-password-required': '新パスワード（確認）を入力してください',
  'password.compare-twice': '新しいパスワードは確認パスワードと一致しません!',
  'password.password-length-validate': '密码必须包含大小写字母和数字，支持英文特殊字符中线和下划线，大于等于8位且小于64位', // TODO 02-02
  'password.twice-password-same-validate': '新しいパスワードは確認パスワードと一致しません!',
  // 地点
  'address.title': '場所管理',
  'address.name': '場所名',
  'address.name-required': '場所名を入力してください',
  'address.id': '場所ID',
  'address.ip': 'IP',
  'address.port': 'ポート',
  'address.add': '新規場所',
  'address.edit': '場所編集',
  'address.stay-open': '地点常开', // TODO 02-02
  'address.visitor-privilege': '来客権限',
  'address.meeting-privilege': '会議権限',
  'address.visitor-privilege-required': '来客権限を選択してください',
  'address.meeting-privilege-required': '会議権限を選択してください',
  'address.include-device': '関連デバイス',
  'address.net-relay': 'ネットリレー',
  'address.relay-channel': 'リレー端子',
  'address.open-door-tip': '全て場所に対して開錠のままにします',
  'address.close-door-tip': '全て場所に対して施錠します',
  'address.all-open': '全て場所に対して常時に開錠しますか（開錠のまま）？',
  'address.all-close': '全て場所に対して施錠しますか？',
  'address.appoint-open': 'に対して常時に開錠しますか（開錠のまま）？',
  'address.appoint-close': 'に対して常時に施錠しますか？',
  'address.select-required': '場所名を入力してください',
  'address.delete-tip': 'この場所を削除しますか？',
  'address.area-management': '区域管理', // TODO 1.28
  'address.area': '区域', // TODO 1.28
  'address.area-floor': '楼层', // TODO 1.28
  'address.area-access-point': '门禁机点位', // TODO 1.28
  'address.area-new': '新增区域', // TODO 1.28
  'address.area-name': '名前', // TODO 1.28
  'address.area-binding-device': '绑定设备', // TODO 1.28
  'address.area-open-error': '{msg}开门发生错误', // TODO 1.28
  'address.area-point-require': '请选择门禁机点位', // TODO 1.28
  'address.area-open-success': '开门成功', // TODO 1.28
  'address.area-close-success': '关门成功', // TODO 1.28
  'address.area-close-error': '{msg}关门发生错误', // TODO 1.28
  'address.area-edit': '编辑区域', // TODO 1.28
  'address.area-edit-end': '结束编辑', // TODO 1.28
  'address.area-if-delete-area': '此操作将永久删除该区域, 是否继续?', // TODO 1.28
  'address.area-deny-delete': '层级下仍有节点，不能删除！', // TODO 1.28
  'address.area-if-delete-point': '此操作将永久删除该点位, 是否继续?', // TODO 1.28
  'address.area-add': '添加区域', // TODO 1.28
  'address.area-add-point': '添加点位', // TODO 1.28
  'address.area-name-require': '请输入名称', // TODO 1.28
  'address.area-edit-point': '编辑点位', // TODO 1.28
  // 验证表达式
  'validateReg.phone': '^(\\+?0?86\\-?)?1[345789]\\d{9}$',
  'validateReg.phone-checked-error': '手机号格式不正确', // TODO 02-02
  // 全局提醒工具类
  'globalRemind.operate-success': '操作成功', // TODO 02-02
  'globalRemind.operate-fail': '操作失败', // TODO 02-02
  'globalRemind.warning': '警告', // TODO 02-02
  'globalRemind.message': '消息', // TODO 02-02
  'globalRemind.success': '成功', // TODO 02-02
  'globalRemind.error': '错误', // TODO 02-02
  'globalRemind.unknow-error': '未知错误' // TODO 02-02
}

export default jpn
