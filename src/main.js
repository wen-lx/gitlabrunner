import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n' // 引入多语言
import ElementUI from 'element-ui'
import GlobalRemind from '@utils/pageHandle/globalRemind' // 全局提醒
import '@/assets/font-awesome/css/font-awesome.min.css' // 图标库
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/css/init.css' // 初始化css样式
import '@/styles/stylus/element-ui.stylus' // 初始化element-ui样式
import '@/styles/stylus/custom.stylus' // 自定义样式
import '@/styles/stylus/fade.stylus' // 加载效果
/** 全局注册所有指令 */
import * as directive from '@/utils/directive'
import Card from '@comp/Card/Card'
import QueryBar from '@comp/Bar/QueryBar'
import CardMenuItem from '@comp/Card/CardMenuItem'
import moment from 'moment'

Vue.component('Card', Card)
Vue.component('QueryBar', QueryBar)
Vue.component('CardMenuItem', CardMenuItem)

Object.keys(directive).forEach(key => Vue.directive(key, directive[key]))

Vue.config.productionTip = false

ElementUI.Dialog.props.closeOnClickModal.default = false // 禁止Dialog点击外部关闭
Vue.use(ElementUI, { i18n: (key, value) => i18n.t(key, value), size: 'small' })
Vue.use(GlobalRemind)
// 国际化修改
ElementUI.Select.props.placeholder.default = i18n.t('tips.select-pleaseholder')
ElementUI.TimePicker.mixins[0].props.placeholder.default = i18n.t('tips.select-pleaseholder')
ElementUI.DatePicker.mixins[0].props.placeholder.default = i18n.t('tips.select-pleaseholder')
ElementUI.DatePicker.mixins[0].props.startPlaceholder.default = i18n.t('tips.start-time')
ElementUI.DatePicker.mixins[0].props.endPlaceholder.default = i18n.t('tips.end-time')
localStorage.setItem('localStorageThemeColor', '#4b8e8d')

Vue.prototype.transformToDateTime = (val) => {
  return moment(val).format('YYYY-MM-DD HH:mm:ss')
}

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
