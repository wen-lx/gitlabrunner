import moment from 'moment'
import Big from 'big.js'

export const attendanceMixin = {
  data () {
    return {
      statusMixin: {
        normal: { // 正常 (rate: 统计数量, rate: 统计量)
          desc: this.$t('attendance.normal'),
          color: '#67C23A',
          num: 0,
          rate: 0
        },
        late: { // 迟到
          desc: this.$t('attendance.late'),
          color: '#E6A23C',
          num: 0,
          rate: 0

        },
        leaveEarly: { // 早退
          desc: this.$t('attendance.leave-early'),
          color: '#8566aa',
          num: 0,
          rate: 0
        },
        absenteeism: { // 未打卡
          desc: this.$t('attendance.absenteeism'),
          color: '#909399',
          num: 0,
          rate: 0
        },
        abnormal: { // 异常
          desc: this.$t('attendance.abnormal'),
          color: '#F56C6C',
          num: 0,
          rate: 0
        }
      }
    }
  },
  methods: {
    initStatusOptionsMixin () {
      this.statusMixin = {
        normal: { // 正常
          desc: this.$t('attendance.normal'),
          color: '#67C23A',
          num: 0,
          rate: 0
        },
        late: { // 迟到
          desc: this.$t('attendance.late'),
          color: '#E6A23C',
          num: 0,
          rate: 0

        },
        leaveEarly: { // 早退
          desc: this.$t('attendance.leave-early'),
          color: '#8566aa',
          num: 0,
          rate: 0
        },
        absenteeism: { // 未打卡
          desc: this.$t('attendance.absenteeism'),
          color: '#909399',
          num: 0,
          rate: 0
        },
        abnormal: { // 异常
          desc: this.$t('attendance.abnormal'),
          color: '#F56C6C',
          num: 0,
          rate: 0
        }
      }
    },
    // 获取考勤状态
    getStatusMixin (attendance) {
      const { startTime, endTime, normalStartTime, normalEndTime } = attendance
      const num = (startTime ? 1 : 0) + (endTime ? 1 : 0)
      if (num === 1) return 'abnormal' // 异常
      else if (num === 0) return 'absenteeism' // 未打卡
      else {
        if (startTime >= normalStartTime + 1000 * 60) return 'late' // 迟到
        if (endTime < normalEndTime) return 'leaveEarly' // 早退
        return 'normal' // 正常
      }
    },
    // 计算工作时长
    calcWorkingHoursMixin (startTime, endTime) {
      if (startTime && endTime) {
        const workingHours = moment(endTime).diff(moment(startTime), 'hours', true)
        return this.keepTwoDecimalMixin(workingHours)
      }
      return 0
    },
    // 计算加班时长
    calcOvertimeHoursMixin (startTime, endTime, normalStartTime, normalEndTime) {
      if (startTime && endTime && normalStartTime && normalEndTime) {
        const a = endTime - startTime
        const b = normalEndTime - normalStartTime
        if (a - b > 0) {
          const overtimeHours = moment(a).diff(moment(b), 'hours', true)
          return this.keepTwoDecimalMixin(overtimeHours)
        }
      }
      return 0
    },
    // 保留两位小数
    keepTwoDecimalMixin (num) {
      const temp = new Big(num)
      return Number(temp.toFixed(2))
    }
  }
}
