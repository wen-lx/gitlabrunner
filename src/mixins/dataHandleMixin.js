export const dataHandleMixin = {
  data () {
    return {

    }
  },
  methods: {
    // 根据属性值 查找 列表中拥有相同属性记录的 某个属性值
    handleGetTargetByQueryMixin (val, objList, queryParamName, targetParamName) {
      // 存在目标值属性时 查找目标值 否则返回item
      if (targetParamName) {
        for (let item of objList) {
          if (item[targetParamName]) {
            if (item[queryParamName] === val) {
              return item[targetParamName]
            }
          }
        }
        console.warn('Can not get targetParamName!')
        return false
        // return false
      } else {
        for (let item of objList) {
          if (item[queryParamName] === val) {
            return item
          }
        }
        // 查询不到 则返回{}
        console.warn('Can not get item!')
        return {}
      }
    },
    // 过滤所有删除、停权的子项信息
    handleFilterArrayByStatusMixin (array) {
      // 数组类型判断
      if (!Array.isArray(array)) {
        console.error('请检查过滤内容是否为数组！')
        return []
      }
      let finalLeaveTypeList = []
      // 过滤停权、删除状态的子项
      for (let item of array) {
        // if (item.status !== 9) { // 这里过滤删除状态的数据
        if (item.status === 2) {
          item.disabled = true
        }
        finalLeaveTypeList.push(item)
        // }
      }
      return finalLeaveTypeList
    },
    // 获取准确数据类型
    getTypeMixin (val) {
      let type = typeof val
      if (type !== 'object') {
        return type
      }
      return Object.prototype.toString.call(val).replace(/^\[object (\S+)\]$/, '$1')
    },
    // 比较新、旧对象数组 根据参数名找出需要 【删除的参数值数组】 和 【新增的对象数组】
    getDifferenceByTwoArray (oldArray, newArry, paramName) {
      let finalObj = {
        addArray: [],
        delParams: []
      }
      // 提取对象数组中指定的属性值 生成obj
      const oldParamObj = this.getParamNamesByObjArray(oldArray, paramName)
      const newParamObj = this.getParamNamesByObjArray(newArry, paramName)

      // 循环旧数组 查找需要删除的params
      for (let item of oldArray) {
        if (!newParamObj[item[paramName]]) { // 不存在则 添加该参数值
          finalObj.delParams.push(item[paramName])
        }
      }
      // 循环新数组 查找需要新增的objArray
      for (let item of newArry) {
        if (!oldParamObj[item[paramName]]) { // 不存在则 添加该对象
          finalObj.addArray.push(item)
        }
      }
      // console.log(finalObj)
      return finalObj
    },
    // 提取对象数组中指定的属性值至obj中以供比较
    getParamNamesByObjArray (array, paramName) {
      let paramObj = {}
      for (let item of array) {
        if (item[paramName]) { // 当参数存在时提取
          paramObj[item[paramName]] = true
        }
      }
      return paramObj
    },
    // 生成随机字符串
    randomGenerate (length) {
      const chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
        'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
      if (!Number.isInteger(length) || length <= 0) { // 合法性校验
        console.error('请检查输入随机字符串长度是否为正整数！')
        return 'Error'
      }
      let randomString = ''
      for (let i = 0; i < length; i++) {
        randomString += chars[Math.floor(Math.random() * chars.length)]
      }
      return randomString
    }
  }
}
