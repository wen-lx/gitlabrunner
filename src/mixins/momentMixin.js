import moment from 'moment'

export const momentMixin = {

  data () {
    return {
      meetingStatusOptionsMixin: {
        noStart: {
          status: 0,
          type: 'warning',
          desc: this.$t('attendance.not-start')
        },
        onGoing: {
          status: 1,
          type: 'success',
          desc: this.$t('attendance.ongoing')
        },
        finished: {
          status: 2,
          type: 'default',
          desc: this.$t('attendance.finished')
        }
      }
    }
  },

  methods: {
    // 格式化日期显示
    momentFormatMixin (val, str) {
      return moment(val).format(str)
    },
    // 获取较早时间,包含当前时间对比
    // startTime endTime 为 moment() / Date() / 2020-10-01 08:00
    // format 返回值显示格式
    getEarlyTimeMixin (startTime, endTime, str = 'HH:mm') {
      let earlyTime = null
      try {
        if (moment(startTime).isBefore(moment())) {
          earlyTime = moment()
        } else if (moment(startTime).isAfter(moment(endTime))) {
          earlyTime = moment(endTime)
        } else {
          earlyTime = moment(startTime)
        }
        return earlyTime.format(str)
      } catch (error) {
        this.$toast.fail(this.$t('时间校验异常'))
        console.log(error)
      }
    },
    // 获取较晚时间,不包含当前时间对比
    // startTime endTime 为 moment() / Date() / 2020-10-01 08:00
    // format 返回值显示格式
    getLateTimeMixin (startTime, endTime, str = 'HH:mm') {
      let lateTime = null
      try {
        if (moment(endTime).isBefore(moment(startTime))) {
          lateTime = moment(startTime)
        } else {
          lateTime = moment(endTime)
        }
        return lateTime.format(str)
      } catch (error) {
        this.$toast.fail(this.$t('时间校验异常'))
        console.log(error)
      }
    },
    // 获取当前周几
    getDayInWeekMixin (val) {
      const day = this.momentFormatMixin(val, 'd')
      switch (day) {
        case '1':
          return this.$t('星期一')
        case '2':
          return this.$t('星期二')
        case '3':
          return this.$t('星期三')
        case '4':
          return this.$t('星期四')
        case '5':
          return this.$t('星期五')
        case '6':
          return this.$t('星期六')
        case '0':
          return this.$t('星期日')
        default:
          return 'date or time error'
      }
    },
    // 获取最近几天显示 (目前支持 今天、明天)
    getNewFewDayMixin (val) {
      if (moment(val).isBetween(moment().startOf('day'), moment().endOf('day'))) {
        return this.$t('attendance.today')
      }
      if (moment(val).isBetween(moment().add(1, 'day').startOf('day'), moment().add(1, 'day').endOf('day'))) {
        return this.$t('attendance.tomorrow')
      }
      return null
    },
    // 获取会议状态
    getMeetingStatusMixin (startTime, endTime) {
      if (moment().isBefore(startTime)) {
        return this.meetingStatusOptionsMixin.noStart
      } else if (moment().isAfter(endTime)) {
        return this.meetingStatusOptionsMixin.finished
      }
      return this.meetingStatusOptionsMixin.onGoing
    }
  }
}
