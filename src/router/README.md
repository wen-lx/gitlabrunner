# 侧边导航生成说明

## 简介
> 侧边导航栏和面包屑由路由自动生成，添加新页面和对应功能菜单，仅需要在路由中添加对应路由信息和状态即可。(具体请参见`配置信息`)

## 配置说明
- 设置主路由`meta`中的`title`和`iconClass`对应于一级菜单的标题和图标
- 设置子路由`meta`中的`title`和`role`对应于二级菜单的标题和权限
- 当后台设置管理员某功能无`list`权限时，会隐藏该功能对应的二级菜单
- 需要手动隐藏二级菜单时，注释该二级菜单的`meta`信息即可

## 示例
```js
const routes = [
  // 系统配置相关路由
  {
    path: '/system',
    redirect: '/system/system-config',
    // 对应一级菜单名称为 '系统信息'，且图标为 'el-icon-s-tools'
    meta: {
      title: '系统信息',
      iconClass: 'el-icon-s-tools'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/system/system-config',
        name: 'system-config',
        // 对应一级菜单名称为 '系统配置'，权限为 'system'
        meta: { title: '系统配置', role: 'system' },
        component: () => import('@views/SystemInfo/SystemConfig.vue')
      },
      {
        path: '/system/device-type-manage',
        name: 'device-type-manage',
        // 注释该行即可隐藏该二级菜单
        // meta: { title: '设备类型', role: 'deviceType' },
        component: () => import('@views/SystemInfo/DeviceTypeManage.vue')
      }
      // 此处可添加更多同类二级菜单
    ]
  }
  // 此处可添加更多一级菜单
]
```