import Vue from 'vue'
import VueRouter from 'vue-router'
import { asyncRouterMap } from './router.config.js'
import i18n from '@/i18n/index' // 引入多语言
// 页面加载进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
// 引入状态管理
import store from '@/store/index.js'

Vue.use(VueRouter)

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: asyncRouterMap
})

// 消除点击相同菜单栏 控制台报错的影响
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

// 路由变动前启动NProgress
NProgress.configure({ showSpinner: false })
router.beforeEach((to, from, next) => {
  let hasPermission = false
  if (sessionStorage.getItem('namespace') === 'user') {
    // 机构管理员登录，不展示没有后台权限的页面
    let permission = []
    try {
      permission = JSON.parse(sessionStorage.getItem('permission')).map(item => item.alias)
    } catch {
      next({ name: 'login' })
      return
    }
    hasPermission = to.meta.role && !permission.includes(to.meta.role)
  } else {
    // 超管登录，不展示考勤，会议等页面
    hasPermission = to.meta.isAdmin === undefined
  }
  // 跳转login页面时无需校验
  if (['login', 'admin-login'].indexOf(to.name) === -1) {
    if (hasPermission) {
      next({ name: 'Exception', params: { type: 403 } })
    } else {
      store.commit('CHNAGE_BREADCRUMB', to.path)
      NProgress.start()
      next()
    }
  } else {
    NProgress.start()
    next()
  }
})
// 路由变动后结束NProgress
router.afterEach(() => {
  NProgress.done()
  // 中科视拓
  document.title = i18n.t('app.智慧园区企业版 v2.1.1')
  // 中科院计算所
  // document.title = i18n.t('app.中科院计算所智慧园区')
})

export default router
