import i18n from '@/i18n/index' // 引入多语言

export const asyncRouterMap = [
  // 根路由
  {
    path: '/',
    redirect: '/login'
  },
  // 登录路由
  {
    path: '/login',
    name: 'login',
    component: () => import('@views/Login/Login.vue')
  },
  // 超管登录路由
  {
    path: '/admin-login',
    name: 'admin-login',
    component: () => import('@views/Login/AdminLogin.vue')
  },
  // 欢迎页
  {
    path: '/welcome',
    redirect: '/welcome',
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/welcome',
        name: 'welcome',
        meta: { isSuperAdmin: 'hidden', isAdmin: 'admin' },
        component: () => import('@views/Welcome/Welcome.vue')
      }
    ]
  },
  // 系统配置相关路由
  {
    path: '/system',
    redirect: '/system/system-config',
    meta: {
      title: i18n.t('app.system-info'),
      iconClass: 'el-icon-s-tools'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/system/system-config',
        name: 'system-config',
        meta: { title: i18n.t('app.system-config'), isAdmin: 'admin', role: 'getSystemList' },
        component: () => import('@views/SystemInfo/SystemConfig.vue')
      },
      {
        path: '/system/device-type-manage',
        name: 'device-type-manage',
        meta: { title: i18n.t('app.device-type'), isAdmin: 'admin' },
        component: () => import('@views/SystemInfo/DeviceTypeManage.vue')
      }
    ]
  },
  // 权限配置相关路由
  {
    path: '/privilege',
    redirect: '/privilege/privilege-manage',
    meta: {
      title: i18n.t('app.permission-config'),
      iconClass: 'el-icon-s-promotion'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/privilege/privilege-manage',
        name: 'privilege-manage',
        meta: { title: i18n.t('app.permission-manage'), isAdmin: 'admin', role: 'createPrivilegeGroups' },
        component: () => import('@views/PrivilegeInfo/PrivilegeManage.vue')
      },
      {
        path: '/privilege/identity-manage',
        name: 'privilege-identity-manage',
        meta: { title: i18n.t('app.identity-manage'), isAdmin: 'admin', role: 'getIdentityList' },
        component: () => import('@views/PrivilegeInfo/IdentityManage.vue')
      }
    ]
  },
  // 机构配置相关路由
  {
    path: '/organization',
    redirect: '/organization/organization-manage',
    meta: {
      title: i18n.t('app.organization-config'),
      iconClass: 'el-icon-share'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/organization/organization-manage',
        name: 'organization-manage',
        meta: { title: i18n.t('app.organization-manage'), isAdmin: 'admin', role: 'getOrganizationList' },
        component: () => import('@views/OrganizationInfo/OrganizationManage.vue')
      },
      {
        path: '/organization/group-manage',
        name: 'group-manage',
        meta: { title: i18n.t('app.level-manage'), isAdmin: 'admin', role: 'getLevelsList' },
        component: () => import('@views/OrganizationInfo/GroupManage.vue')
      }
    ]
  },
  // 人员信息相关路由
  {
    path: '/person',
    redirect: '/person/member-manage',
    meta: {
      title: i18n.t('member.info'),
      iconClass: 'el-icon-user-solid'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/person/member-manage',
        name: 'member-manage',
        meta: { title: i18n.t('member.management'), isAdmin: 'admin', role: 'getUserList', showGroupManage: 'show' },
        component: () => import('@views/PersonInfo/MemberManage.vue')
      },
      {
        path: '/person/member-invitation',
        name: 'member-invitation',
        meta: { title: '', isAdmin: 'admin', role: 'getUserList', showGroupManage: 'show' },
        component: () => import('@views/PersonInfo/MemberInvitation.vue')
      },
      {
        path: '/person/visitor-apply-manage',
        name: 'visitor-apply-manage',
        meta: { title: i18n.t('member.visitor.appointment'), isAdmin: 'admin', role: 'getVisitorApplyList', showGroupManage: 'show' },
        component: () => import('@views/PersonInfo/VisitorApplyManage.vue')
      }
    ]
  },
  // 设备信息相关路由
  {
    path: '/device',
    redirect: '/device/device-manage',
    meta: {
      title: i18n.t('app.device-info'),
      iconClass: 'el-icon-s-platform'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/device/device-manage',
        name: 'device-manage',
        meta: { title: i18n.t('app.device-manage'), isAdmin: 'admin', role: 'getDeviceList' },
        component: () => import('@views/DeviceInfo/DeviceManage.vue')
      },
      {
        path: '/device/device-style-manage',
        name: 'device-style-manage',
        meta: { title: i18n.t('app.device-style-manage'), isAdmin: 'admin', role: 'getStylesList' },
        component: () => import('@views/DeviceInfo/StyleManage.vue')
      },
      {
        path: '/device/time-template-manage',
        name: 'time-template-manage',
        meta: { title: i18n.t('app.time-template'), isAdmin: 'admin', role: 'getTimeTemplateList' },
        component: () => import('@views/DeviceInfo/TimeTemplateManage.vue')
      }
    ]
  },
  // 考勤信息相关路由
  {
    path: '/attendance',
    redirect: '/attendance/attendance-config',
    meta: {
      title: i18n.t('attendance.info'),
      iconClass: 'el-icon-s-check'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/attendance/attendance-config',
        name: 'attendance-config',
        meta: { title: i18n.t('attendance.management'), role: 'getAttendancesList' },
        component: () => import('@views/AttendanceInfo/AttendanceManage.vue')
      }
    ]
  },
  // 记录、日志相关路由
  {
    path: '/record',
    redirect: '/record/pass-record',
    meta: {
      title: i18n.t('app.record-log'),
      iconClass: 'el-icon-s-data'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/record/pass-record',
        name: 'pass-record',
        meta: { title: i18n.t('app.traffic-record'), isAdmin: 'admin', role: 'getPassRecordList', showGroupManage: 'show' },
        component: () => import('@views/RecordInfo/PassRecord.vue')
      },
      {
        path: '/record/device-log',
        name: 'device-log',
        meta: { title: i18n.t('app.device-log'), isAdmin: 'admin', role: 'getDeviceLogsList' },
        component: () => import('@views/RecordInfo/DeviceLog.vue')
      },
      {
        path: '/record/admin-log',
        name: 'admin-log',
        meta: { title: i18n.t('app.operation-log'), isAdmin: 'admin', role: 'getAdminLogsList' },
        component: () => import('@views/RecordInfo/AdminLog.vue')
      }
    ]
  },
  // 会议信息相关路由
  {
    path: '/meeting',
    redirect: '/meeting/meeting-room-manage',
    meta: {
      title: i18n.t('meeting.room.management'),
      iconClass: 'el-icon-s-management'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/meeting/meeting-room-manage',
        name: 'meeting-room-manage',
        meta: { title: i18n.t('meeting.room.management'), role: 'getConferenceRoomsList' },
        component: () => import('@views/MeetingInfo/MeetingRoomManage.vue')
      },
      {
        path: '/meeting/meeting-manage',
        name: 'meeting-manage',
        meta: { title: i18n.t('meeting.management'), role: 'getReservationList' },
        component: () => import('@views/MeetingInfo/MeetingManage.vue')
      }
    ]
  },
  // 地点相关路由
  {
    path: '/address',
    redirect: '/address/address-manage',
    meta: {
      title: i18n.t('app.address-open'),
      iconClass: 'el-icon-s-home'
    },
    component: () => import('@views/Home.vue'),
    children: [
      {
        path: '/address/address-manage',
        name: 'address-manage',
        meta: { title: i18n.t('app.address-manage'), isAdmin: 'admin', role: 'getAddressList' },
        component: () => import('@views/AddressInfo/AddressManage.vue')
      },
      {
        path: '/address/area-manage',
        name: 'area-manage',
        meta: { title: '区域管理', role: 'createAreaGroup' },
        component: () => import('@views/AddressInfo/AreaManage.vue')
      }
    ]
  },
  // 匹配不到，展示404
  {
    path: '/*',
    name: 'Exception',
    meta: { isAdmin: 'admin' },
    component: () => import('@comp/Exception/Exception')
  }
]
