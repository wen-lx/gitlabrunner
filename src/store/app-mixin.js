import { mapState } from 'vuex'

const appMixin = {
  computed: {
    ...mapState({
      isCollapse: state => state.app.isCollapse,
      breadcrumb: state => state.app.breadcrumb,
      activeMenuPath: state => state.app.activeMenuPath
    })
  }
}

export default appMixin
