const getters = {
  isCollapse: state => state.app.isCollapse,
  breadcrumb: state => state.app.breadcrumb,
  activeMenuPath: state => state.app.activeMenuPath
}

export default getters
