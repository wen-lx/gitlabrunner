import Vue from 'vue'
import Vuex from 'vuex'
import { membersApi } from '@/api'
import getters from './getters'
import app from './modules/app'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isSubmitting: false, // 是否表单提交中
    mqttClient: {}, // mqtt客户端订阅
    routerList: [], // 路由列表
    currentOrganization: {}, // 当前机构
    organizations: [], // 用户所在机构列表
    isSuperAdmin: false, // 是否为超级管理员
    permission: [], // 机构管理员和用户权限
    wss: null,
    blob: false,
    WebSocketMessage: null,
    level: 0
  },
  mutations: {
    changeRequestHeaderAccept (state) {
      state.blob = !state.blob
    },
    changeIsSuperAdmin (state) { // 判断是否为超级管理员
      state.isSuperAdmin = sessionStorage.namespace === 'admin'
    },
    setLevel (state, level) {
      state.level = level
    },
    setCurrentOrganizations (state, organization) {
      if (organization) {
        sessionStorage.setItem('currentOrganization', JSON.stringify(organization))
        state.currentOrganization = organization
      } else {
        state.currentOrganization = sessionStorage.currentOrganization !== 'undefined' ? JSON.parse(sessionStorage.currentOrganization) : null
      }
    },
    setOrganizations (state) {
      state.organizations = sessionStorage.organizations && JSON.parse(sessionStorage.organizations)
    },
    closeIsSubmitting (state) { // 关闭提交状态
      state.isSubmitting = false
    },
    openIsSubmitting (state) { // 开启提交状态
      state.isSubmitting = true
    },
    changePermission (state) { // 更新权限
      state.permission = sessionStorage.permission && JSON.parse(sessionStorage.permission)
    }
  },
  actions: {
    async changePermission (context) { // 异步获取用户权限
      if (!context.state.currentOrganization) return
      const res = await membersApi.membersIdRoleGet(context.state.currentOrganization.memberId)
      if (res.data) {
        sessionStorage.setItem('permission', JSON.stringify(res.data.actionResources))
        context.commit('changePermission')
      }
    }
  },
  modules: {
    app
  },
  getters
})
