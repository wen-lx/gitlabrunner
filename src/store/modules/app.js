const app = {
  state: {
    isCollapse: false, // 侧边栏状态
    breadcrumb: [], // 面包屑状态
    activeMenuPath: '' // 侧边栏激活状态路径
  },

  mutations: {
    CHANGE_IS_COLLAPSE: (state) => { // 改变侧边栏状态
      state.isCollapse = !state.isCollapse
    },
    CHNAGE_BREADCRUMB: (state, toPath) => { // 修改面包屑内容
      state.activeMenuPath = toPath
      state.breadcrumb = toPath.substr(1).split('/') // 去除第一个/ 并按照/切割
    }
  },

  actions: {

  }
}

export default app
