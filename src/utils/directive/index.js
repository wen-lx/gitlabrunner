
/**
 * 根据某一字段（id）显示另一字段（name）
 * */
export const showName = (el, binding) => {
  let value = binding.value.value || 'id'
  let label = binding.value.label || 'name'
  for (let val of binding.value.list) {
    if (val[value] === binding.value.id) {
      el.innerHTML = val[label]
      return
    }
  }
}

/**
 * 根据权限判断 el 是否显示
 * */
export const privilege = async (el, binding) => {
  let namespace = sessionStorage.getItem('namespace')
  let privileges = sessionStorage.getItem('permission')
  let privilege = binding.value
  privileges = privileges ? JSON.parse(privileges) : null
  if (namespace === 'admin') {
    return
  }
  let flag = privileges.some(item => {
    return item.alias === privilege && +item.viewType === 1
  })
  if (!flag) {
    el.style.display = 'none'
    return false
  }
  return true
}
