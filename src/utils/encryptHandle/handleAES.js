import CryptoJS from 'crypto-js'
// 使用sessionStorage中authKey、authIv进行加密
function aesEncryptByLocal (val) {
  try {
    // 生成AES 密钥和向量
    let key = CryptoJS.enc.Utf8.parse(sessionStorage.authKey) // AES密钥
    let iv = CryptoJS.enc.Utf8.parse(sessionStorage.authIv) // AES向量
    // AES私钥加密 设置iv、模式及填充方式
    const aesOptions = { iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }
    return CryptoJS.AES.encrypt(val, key, aesOptions).toString()
  } catch (e) {
    console.error(e)
  }
}

export { aesEncryptByLocal }
