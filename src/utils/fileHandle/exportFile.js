import json2csv from 'json2csv'

// 导出csv文件
function exportCSV (data, fields, fieldNames, fileName) {
  try {
    var result = json2csv({
      data: data,
      fields: fields,
      fieldNames: fieldNames,
      excelStrings: true
    })
    let csvContent = 'data:text/csv;charset=GBK,\uFEFF' + result
    let encodedUri = encodeURI(csvContent)
    let link = document.createElement('a')
    link.setAttribute('href', encodedUri)
    link.setAttribute('download', `${(fileName || 'file')}.csv`)
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  } catch (err) {
    console.error(err)
  }
}

export { exportCSV }
