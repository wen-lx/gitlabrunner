import i18n from '@/i18n/index' // 引入多语言

// 用户名校验 6位以上 20位以下
function validateUsernameMoreSixAndLessTwenty (rule, value, callback) {
  const reg = /^.{6,20}$/
  if (value && !reg.test(value)) {
    callback(new Error(i18n.t('member.staff.username-limit')))
  } else {
    callback()
  }
}
// 姓名校验 2位以上 20位以下
function validateNameMoreTwoAndLessTwenty (rule, value, callback) {
  const reg = /^.{2,20}$/
  if (value && !reg.test(value)) {
    callback(new Error(i18n.t('member.staff.name-limit')))
  } else {
    callback()
  }
}
// 密码校验 8位以上 64位以下,必须包含大小写字母和数字，支持英文特殊字符中线和下划线
function validatePasswordMoreEightAndLessSixtyFour (rule, value, callback) {
  const reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\-_]{8,64}$/
  if (value && !reg.test(value)) {
    callback(new Error(i18n.t('password.password-length-validate')))
  } else {
    callback()
  }
}
// 设备名校验 30位以下
function validateDeviceNameLessThirty (rule, value, callback) {
  const reg = /^.{0,30}$/
  if (value && !reg.test(value)) {
    callback(new Error(i18n.t('device.name-validate')))
  } else {
    callback()
  }
}

function validatePhone (rule, value, callback) {
  // const reg = new RegExp(i18n.t('validateReg.phone'))
  const reg = /^(?:(?:\+|00)86)?1(?:(?:3[\d])|(?:4[5-7|9])|(?:5[0-3|5-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\d])|(?:9[1|8|9]))\d{8}$/
  if (value && !reg.test(value)) {
    callback(new Error(i18n.t('validateReg.phone-checked-error')))
  } else {
    callback()
  }
}

export { validateUsernameMoreSixAndLessTwenty, validateNameMoreTwoAndLessTwenty, validatePasswordMoreEightAndLessSixtyFour, validateDeviceNameLessThirty, validatePhone }
