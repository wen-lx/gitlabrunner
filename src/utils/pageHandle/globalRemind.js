// 全局提醒工具类
import i18n from '@/i18n/index' // 引入多语言

const globalRemind = {
  install (Vue, option) {
    // 成功消息
    Vue.prototype.$handleSuccessMessage = msg => {
      Vue.prototype.$message({
        type: 'success',
        message: msg || i18n.t('globalRemind.operate-success'),
        duration: 1500
      })
    }
    // 失败消息
    Vue.prototype.$handleErrorMessage = msg => {
      Vue.prototype.$message({
        type: 'error',
        message: msg || i18n.t('globalRemind.operate-fail'),
        duration: 1500
      })
    }
    // 警告消息
    Vue.prototype.$handleWarningMessage = (msg, duration) => {
      Vue.prototype.$message({
        type: 'warning',
        message: msg || i18n.t('globalRemind.warning'),
        duration: duration !== null ? duration : 1500
      })
    }
    // 信息消息
    Vue.prototype.$handleInfoMessage = msg => {
      Vue.prototype.$message({
        type: 'info',
        message: msg || i18n.t('globalRemind.message'),
        duration: 1500
      })
    }
    // 成功通知
    Vue.prototype.$handleSuccessNotify = (msg, title, duration) => {
      Vue.prototype.$notify({
        type: 'success',
        title: title || i18n.t('globalRemind.success'),
        message: msg || i18n.t('globalRemind.operate-success'),
        duration: duration !== null ? duration : 1500
      })
    }
    // 错误通知
    Vue.prototype.$handleErrorNotify = (msg, title, duration) => {
      Vue.prototype.$notify({
        type: 'error',
        title: title || i18n.t('globalRemind.error'),
        message: msg || i18n.t('globalRemind.unknow-error'),
        duration: duration !== null ? duration : 1500
      })
    }
    // 警告通知
    Vue.prototype.$handleWarningNotify = (msg, title, duration) => {
      Vue.prototype.$notify({
        type: 'warning',
        title: title || i18n.t('globalRemind.warning'),
        message: msg,
        dangerouslyUseHTMLString: true,
        duration: duration !== null ? duration : 1500
      })
    }
    // 消息通知
    Vue.prototype.$handleInfoNotify = (msg, title, duration) => {
      Vue.prototype.$notify({
        type: 'info',
        title: title || i18n.t('globalRemind.message'),
        message: msg,
        duration: duration !== null ? duration : 1500
      })
    }
  }
}

export default globalRemind
