const path = require('path')
const merge = require('webpack-merge')

function resolve (dir) {
  return path.join(__dirname, dir)
}
const webpack = require('webpack')

module.exports = {
  // 根路径 默认使用/ vue cli 3.3+ 弃用 baseUrl
  publicPath: './',
  // 输出目录
  outputDir: 'dist',
  assetsDir: 'assets',

  configureWebpack: {
    plugins: [
      // Ignore all locale files of moment.js
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new webpack.BannerPlugin('Build by 打酱油')
    ]
  },

  // 链式配置
  chainWebpack: (config) => {
    // 临时修改
    const imagesTempRule = config.module.rule('imagesTemp')
    imagesTempRule
      .include
      .add(resolve('src/assets/images/login-logo.png'))
      .add(resolve('src/assets/images/logo.png'))
      .end()
      .test(/\.(png|jpe?g|gif|webp)(\?.*)?$/)
      .use('file-loader')
      .loader('file-loader')
      .tap(options =>
        merge(options, { name: 'assets/img/[name].[ext]' })
      )

    const imagesRule = config.module.rule('images')
    imagesRule
      .exclude
      .add(resolve('src/assets/images/login-logo.png'))
      .add(resolve('src/assets/images/logo.png'))
      .end()
      .test(/\.(png|jpe?g|gif|webp)(\?.*)?$/)
      .use('url-loader')
      .loader('url-loader')

    config.resolve.alias
      .set('@utils', resolve('src/utils'))
      .set('@assets', resolve('src/assets'))
      .set('@comp', resolve('src/components'))
      .set('@views', resolve('src/views'))
      .set('@excel', resolve('src/excel'))
      .end().end()
      .resolve.extensions.add('.ts').add('.tsx').add('.js')
      .end().end()
      .module
      .rule('typescript')
      .test(/\.tsx?$/)
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('ts-loader')
      .loader('ts-loader')
      .options({
        transpileOnly: true,
        appendTsSuffixTo: [
          '\\.vue$'
        ],
        happyPackMode: false
      })
      .end()
  },

  productionSourceMap: false // Disable during development
}
